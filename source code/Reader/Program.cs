﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace Reader
{
    class Program
    {


        static void Main(string[] args)
        {
            byte[] msg;
            IPEndPoint endp = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000);
            UdpClient udpClient = new UdpClient(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5000));
     
            while (true)
                if (udpClient.Available != 0)
                {
                    msg = udpClient.Receive(ref endp); Console.WriteLine(GetString(msg));
                }  
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
