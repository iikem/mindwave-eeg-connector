﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _vsn_EEGConnector
{
    public partial class Form1 : Form
    {
        private VSNetwork.Hardware.EEG.MindWave mindWave;

        public Form1()
        {

            InitializeComponent();
      
            menuStrip1.Renderer = new MenuStripRenderer();
            this.ResizeRedraw = true;
            this.cOM1ToolStripMenuItem.Checked = true;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            textBox1.Enabled = false;
            notifyIcon1.Visible = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState && minimizeToTrayToolStripMenuItem.Checked==true)
            {
                this.Hide();
                notifyIcon1.Visible = true;
            }
        }
        private void Form1_Close(object sender, FormClosingEventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                this.Hide();
                notifyIcon1.Visible = true;
                //WindowState = FormWindowState.Minimized;
            }
        }
        private void UpdateComStatus()
        {
            if (this.cOM1ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM1"; return; }
            if (this.cOM2ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM2"; return; }
            if (this.cOM3ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM3"; return; }
            if (this.cOM4ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM4"; return; }
            if (this.cOM5ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM5"; return; }
            if (this.cOM6ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM6"; return; }
            if (this.cOM7ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM7"; return; }
            if (this.cOM8ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM8"; return; }
            if (this.cOM9ToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM9"; return; }
            if (this.otherToolStripMenuItem.Checked == true) { toolStripStatusLabel2.Text = "COM1"; return; }
            toolStripStatusLabel2.Text = "NONE"; return;
        }

        private class MenuStripRenderer : ToolStripProfessionalRenderer
        {
            public MenuStripRenderer() : base(new MenuStripColors()) { }
        }

        private class MenuStripColors : ProfessionalColorTable
        {
            public override Color MenuItemSelected { get { return Color.Gray; } }
            public override Color MenuItemSelectedGradientBegin { get { return Color.Gray; } }
            public override Color MenuItemSelectedGradientEnd { get { return Color.Gray; } }
            public override Color MenuItemBorder { get { return Color.DarkGray; } }
            public override Color MenuItemPressedGradientBegin { get { return Color.FromArgb(50, 50, 50); } }
            public override Color MenuItemPressedGradientEnd { get { return Color.FromArgb(50, 50, 50); } }
            public override Color ToolStripContentPanelGradientBegin { get { return Color.FromArgb(50, 50, 50); } }
            public override Color ToolStripDropDownBackground { get { return Color.FromArgb(50, 50, 50); } }
            public override Color ToolStripBorder { get { return Color.DarkGray; } }
            public override Color ImageMarginGradientBegin { get { return Color.FromArgb(50, 50, 50); } }
            public override Color ImageMarginGradientEnd { get { return Color.FromArgb(50, 50, 50); } }
            public override Color ImageMarginGradientMiddle { get { return Color.FromArgb(50, 50, 50); } }
        }

        private void minimizeToTrayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.minimizeToTrayToolStripMenuItem.Checked = !this.minimizeToTrayToolStripMenuItem.Checked;
        }

        private void cOM1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM1ToolStripMenuItem.Checked = !this.cOM1ToolStripMenuItem.Checked;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM2ToolStripMenuItem.Checked = !this.cOM2ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM3ToolStripMenuItem.Checked = !this.cOM3ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM4ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM4ToolStripMenuItem.Checked = !this.cOM4ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM5ToolStripMenuItem.Checked = !this.cOM5ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM6ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM6ToolStripMenuItem.Checked = !this.cOM6ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM7ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM7ToolStripMenuItem.Checked = !this.cOM7ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM8ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM8ToolStripMenuItem.Checked = !this.cOM8ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void cOM9ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.cOM9ToolStripMenuItem.Checked = !this.cOM9ToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.otherToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void otherToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.otherToolStripMenuItem.Checked = !this.otherToolStripMenuItem.Checked;
            this.cOM1ToolStripMenuItem.Checked = false;
            this.cOM2ToolStripMenuItem.Checked = false;
            this.cOM3ToolStripMenuItem.Checked = false;
            this.cOM4ToolStripMenuItem.Checked = false;
            this.cOM5ToolStripMenuItem.Checked = false;
            this.cOM6ToolStripMenuItem.Checked = false;
            this.cOM7ToolStripMenuItem.Checked = false;
            this.cOM8ToolStripMenuItem.Checked = false;
            this.cOM9ToolStripMenuItem.Checked = false;
            UpdateComStatus();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox1.Text = "127.0.0.1";
                textBox1.Enabled = false;
            }
            else
            {
                textBox1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "Lock")
            {
                toolStripStatusLabel3.Text = "Ready";
                button1.Text = "Unlock";
                checkBox1.Enabled = false;
                textBox1.Enabled = false;
                textBox2.Enabled = false;
            }
            else
            {
                button1.Text = "Lock";
                checkBox1.Enabled = true;
                if(checkBox1.Checked==false) textBox1.Enabled = true;
                textBox2.Enabled = true;
            }
        }

        private void activateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (activateToolStripMenuItem.Text == "Activate")
            {
                if (button1.Text == "Lock")
                {
                    toolStripStatusLabel3.Text = "Lock!!";
                    return;
                }
                VSNetwork.Hardware.EEG.SerialPortName sPort = VSNetwork.Hardware.EEG.SerialPortName.COM1;
                if (toolStripStatusLabel2.Text == "COM1") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM1;
                if (toolStripStatusLabel2.Text == "COM2") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM2;
                if (toolStripStatusLabel2.Text == "COM3") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM3;
                if (toolStripStatusLabel2.Text == "COM4") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM4;
                if (toolStripStatusLabel2.Text == "COM5") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM5;
                if (toolStripStatusLabel2.Text == "COM6") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM6;
                if (toolStripStatusLabel2.Text == "COM7") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM7;
                if (toolStripStatusLabel2.Text == "COM8") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM8;
                if (toolStripStatusLabel2.Text == "COM9") sPort = VSNetwork.Hardware.EEG.SerialPortName.COM9;
                mindWave = new VSNetwork.Hardware.EEG.MindWave(sPort, Convert.ToInt32(textBox2.Text));
               if(textBox1.Text!="127.0.0.1") mindWave.SetIPAddress(textBox1.Text);
               try
               {
                   mindWave.Connect();
                   toolStripStatusLabel3.Text = "Active";
                   activateToolStripMenuItem.Text = "Deactivate";
               }
               catch
               {
                   toolStripStatusLabel3.Text = "Error";
               }
               }
            else
            {
                activateToolStripMenuItem.Text = "Activate";
                mindWave.Disconnect();
                toolStripStatusLabel3.Text = "Disabled";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                mindWave.Disconnect();
            }
            catch (Exception ex)
            {
            }
            Application.Exit();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
 
        }

        private void versionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            (new Form2()).ShowDialog();
        }
    }
}
