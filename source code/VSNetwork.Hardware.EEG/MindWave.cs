﻿using System;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Threading;
namespace VSNetwork.Hardware.EEG
{
    public enum SerialPortName
    {
        COM1, COM2, COM3, COM4, COM5, COM6, COM7, COM8, COM9, COM10, COM11, COM12, COM13, COM14, COM15, COM16, COM17, COM18, COM19, COM20
    }

    public class MindWave
    {
        #region Objects & Variables
            private byte[] payload = new byte[173];
            private byte pLength;
            private int currPoint = 0;
            private string port = "";
            private SerialPort serialPort;
            private UdpClient udpClient;
            private IPEndPoint rawData;
            private bool isSync1 = false;
            private bool isSync2 = false;
            private bool ispLength = false;
            private int checksum;
            private int UDPport = 0;
        #endregion Objects & Variables

        #region Classes
            protected static class Messages
            {
                public static readonly byte[] reqConnect;
                public static readonly byte[] reqDisconnect = { 193 };
                public static readonly byte[] reqAutoConnect = { 194 };
                public static readonly byte[] recSync = { 170 };
            }
        #endregion Classes

        #region Constructors
            /// <summary>
            /// Creates an instance of Neurosky MindWave
            /// </summary>
            /// <param name="commPort">Set COM Port</param>
            /// <param name="UDPPort">Set UDP Port</param>
            public MindWave(SerialPortName commPort, int UDPPort)
            {
                this.port = commPort.ToString();
                serialPort = new SerialPort(this.port, 115200, Parity.None, 8, StopBits.One);
                this.rawData = new IPEndPoint(IPAddress.Parse("127.0.0.1"), UDPPort);
                udpClient = new UdpClient();
            }
        #endregion Constructors

        #region Methods
            #region Public
                /// <summary>
                /// Setting IP Address for UDP Messages
                /// </summary>
                /// <param name="IP">Set IP Address for Packets</param>
                public void SetIPAddress(string IP)
                {
                    this.rawData = new IPEndPoint(IPAddress.Parse(IP), UDPport);
                }
                /// <summary>
                /// Starts receiving Data and Broadcasting them via UDP
                /// </summary>
                public void Connect()
                {
                    if (serialPort.IsOpen == true) { Disconnect(); return; }
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
                    serialPort.ReadBufferSize = 5000;
                    serialPort.Open();
                    serialPort.Write(Messages.reqAutoConnect, 0, Messages.reqAutoConnect.Length);
                }
                /// <summary>
                /// Disconnect and Close any active connection
                /// </summary>
                public void Disconnect()
                {
                    serialPort.Write(Messages.reqDisconnect, 0, Messages.reqDisconnect.Length);
                    Thread.Sleep(15);
                    serialPort.Close();
                }
            #endregion Public
            #region Private
                private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
                {
                    try
                    {
                        while (true)
                        {
                            byte recByte = (byte)serialPort.ReadByte();

                            if (recByte == Messages.recSync[0] && isSync1 == false) { isSync1 = true; continue; }
                            if (recByte == Messages.recSync[0] && isSync2 == false) { isSync2 = true; continue; }
                            if (isSync1 == true && isSync2 == true && ispLength == false)
                            {
                                if (recByte == 170) continue;
                                if (recByte > 169) { isSync1 = false; isSync2 = false; continue; }
                                pLength = recByte; currPoint = 0; checksum = 0; ispLength = true;
                                continue;
                            }
                            if (isSync1 == true && isSync2 == true && ispLength == true && currPoint < pLength)
                            {
                                payload[currPoint++] = recByte; checksum += recByte; continue;
                            }
                            if (isSync1 == true && isSync2 == true && ispLength == true && currPoint >= pLength)
                            {
                                checksum &= 255; checksum = ~checksum & 255;
                                if (recByte == checksum) ParsingPacket();
                                isSync1 = false; isSync2 = false; ispLength = false; checksum = 0; currPoint = 0; continue;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                private void ParsingPacket()
                {
                    int bytesParsed = 0;
                    byte[] msg;
                    bool parsed = false;

                    while (bytesParsed < pLength)
                    {
                        byte code = payload[bytesParsed++];
                        switch (code)
                        {
                            case 22:
                                {
                                    bytesParsed++;
                                    msg = GetBytes("[BL:" + payload[bytesParsed++] + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    break;
                                }
                            case 128:
                                {
                                    bytesParsed++;
                                    msg = GetBytes("[RW:" + BitConverter.ToInt16(payload, bytesParsed).ToString() + "]");
                                    bytesParsed += 2;
                                    udpClient.Send(msg, msg.Length, rawData);
                                    parsed = true;
                                    break;
                                }
                            case 2:
                                {
                                    msg = GetBytes("[SG:" + payload[bytesParsed++] + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    parsed = true;
                                    break;
                                }
                            case 131:
                                {
                                    bytesParsed++;
                                    byte[] delta={0,payload[bytesParsed++], payload[bytesParsed++],payload[bytesParsed++]};
                                    byte[] theta = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] lalpha = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] halpha = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] lbeta = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] hbeta = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] lgamma = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };
                                    byte[] mgamma = { 0, payload[bytesParsed++], payload[bytesParsed++], payload[bytesParsed++] };       
                                    msg = GetBytes("[DL:" + BitConverter.ToInt32(theta,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[TH:" +  BitConverter.ToInt32(delta,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[LA:" +  BitConverter.ToInt32(lalpha,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[HA:" +  BitConverter.ToInt32(halpha,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[LB:" +  BitConverter.ToInt32(lbeta,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[HB:" +  BitConverter.ToInt32(hbeta,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[LG:" +  BitConverter.ToInt32(lgamma,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    msg = GetBytes("[MG:" +  BitConverter.ToInt32(mgamma,0).ToString() + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    break;
                                }
                            case 4:
                                {
                                    msg = GetBytes("[AT:" + payload[bytesParsed++] + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    break;
                                }
                            case 5:
                                {
                                    msg = GetBytes("[ME:" + payload[bytesParsed++] + "]");
                                    udpClient.Send(msg, msg.Length, rawData);
                                    break;
                                }
                          
                        }
                    }
                    if (parsed == false)
                    {
                        msg = GetBytes("NOT PARSED");
                        udpClient.Send(msg, msg.Length, rawData);
                    }
                }
                private byte[] GetBytes(string str)
                {
                    byte[] bytes = new byte[str.Length * sizeof(char)];
                    System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
                    return bytes;
                }

            #endregion Private
        #endregion Methods
    }
}

